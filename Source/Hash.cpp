#include <cstdlib>
#include <iostream>
#include <string>

#include "../Header/Hash.h"

using namespace std;

Hash::Hash() {
    for (int i = 0; i < tableSize; i++) {
        HashTable[i] = new item;
        HashTable[i]->name = "empty";
        HashTable[i]->drink = "empty";
        HashTable[i]->next = NULL;
    }
}

int Hash::hashFunction(string key) {
    int hash = 0;
    int index = 0;

    for (int i = 0; i < key.length(); i++) {
         //hash += (int)key[i];
         hash += (int)key[i] * 17;
    }

    index = hash % tableSize;

    return index;
}

void Hash::addItem(string name, string drink) {
    int index = hashFunction(name);

    if(HashTable[index]->name == "empty") {
        HashTable[index]->name = name;
        HashTable[index]->drink = drink;
    } else {
        item* curItem = HashTable[index];

        item* newItem = new item;
        newItem->name = name;
        newItem->drink = drink;
        newItem->next = NULL;
        
        while(curItem->next != NULL) {
            curItem = curItem->next;
        }
        curItem->next = newItem;
    }

    std::cout << "Index: " << index << " Name: " << name << std::endl;
}

int Hash::numberOfItemsInIndex(int index) {
    item* curItem = HashTable[index];
    int count = 0;

    if (HashTable[index]->name == "empty") {
        count = 0;
    } else {
        count++;
        while(curItem->next != NULL) {
            count++;
            curItem = curItem->next;
        }
    }

    return count;
}

void Hash::printTable() {
    int number;
    for (int i = 0; i < tableSize; i++) {
        number = numberOfItemsInIndex(i);
        cout << "-------------------\n";
        cout << "index = " << i << endl;
        cout << HashTable[i]->name << endl;
        cout << HashTable[i]->drink << endl;
        cout << "# of items = " << number << endl;
        cout << "-------------------\n\n";
    }
}

void Hash::printItemsInIndex(int index) {
    item* curItem = HashTable[index];

    if(numberOfItemsInIndex(index)) {
        cout << "Index: " << index << " contains: " << endl;

        while(curItem != NULL) {
            cout << "-------------------\n";
            cout << curItem->name << endl;
            cout << curItem->drink << endl;
            cout << "-------------------\n\n";

            curItem = curItem->next;
        }
    } else {
        cout << "Index: " << index << " is empty" << std::endl;
    }
}

void Hash::findDrink(string name) {
    int index = hashFunction(name);
    bool foundName = false;
    string drink;

    item* curItem = HashTable[index];

    while(NULL != curItem && !foundName) {
        if(name == curItem->name) {
            foundName = true;
            drink = curItem->drink;
        }
        curItem = curItem->next;
    }

    if(foundName) {
        std::cout << name << "'s drink is: " << drink << std::endl;
    } else {
        std::cout << name << "'s info was not found." << std::endl;
    }
}

void Hash::removeItem(string name) {
    int index = hashFunction(name);

    item* curItem = HashTable[index];
    item* nextItem;

    if("empty" == curItem->name) {
        //not found
        std::cout << name << " was not found in table" << std::endl;
    } else if(name == curItem->name && NULL == curItem->next) {
        //one item in bucket
        //pseudo delete
        curItem->name = "empty";
        curItem->drink = "empty";
        std::cout << name << " was removed from the table" << std::endl;
    } else if(name == curItem->name && NULL != curItem->next) {
        //first item and more than one item in bucket
        //delete
        nextItem = curItem->next;
        delete curItem;
        std::cout << name << " was removed from the table" << std::endl;
    } else {
        //not first item
        nextItem = curItem->next;

        while(NULL != nextItem && name != nextItem->name) {
            curItem = nextItem;
            nextItem = nextItem->next;

            if(NULL == nextItem) {
                //No match
                std::cout << name << " was not found in table" << std::endl;
            } else {
                //found
                nextItem = curItem->next;
                delete curItem;
                std::cout << name << " was removed from the table" << std::endl;
            }
        }
    }
}

//Refactor multiple class methods to use search
//int Hash::search(string name) {}
