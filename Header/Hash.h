#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

#ifndef HASH_H
#define HASH_H

class Hash {
    public:
        Hash();
        int hashFunction(string key);
        void addItem(string name, string drink);
        int numberOfItemsInIndex(int index);
        void printTable();
        void printItemsInIndex(int index);
        void findDrink(string name);
        void removeItem(string name);

    private:
        static const int tableSize = 10;

        struct item{
            string name;
            string drink;
            item* next;
        };

        item* HashTable[tableSize];
};

#endif