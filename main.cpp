#include <cstdlib>
#include <iostream>
#include <string>

#include "Header/Hash.h"

using namespace std;

int main(int argc, char** argv) {

    Hash* hashTable = new Hash();
    string name = "";

    hashTable->addItem("Callie", "Mocha");
    hashTable->addItem("Josh", "Coffee");
    hashTable->addItem("Mike", "Tea");
    hashTable->addItem("Jones", "Orange Juice");
    hashTable->addItem("Eyes", "Blended Chicken");
    hashTable->addItem("Purple", "Water");
    hashTable->addItem("Text", "Soda");
    hashTable->addItem("String", "Coke");
    hashTable->addItem("Test", "Frap");
    hashTable->addItem("Debug", "Boiling Water");
    hashTable->addItem("Thisisanothername", "Chicken Broth");

    hashTable->printTable();
    hashTable->removeItem("Jones");
    hashTable->printTable();

    // while(name != "exit") {
    //     std::cout << "Search for: ";
    //     std::cin >> name;
    //     if(name != "exit") {
    //         hashTable->findDrink(name);
    //     }
    // }

    // hashTable->printItemsInIndex(6);
    // hashTable->printItemsInIndex(7);
    // hashTable->printItemsInIndex(9);
    //hashTable->printTable();

    return 0;
}
